const mongoose = require("mongoose");
const Course = require("../models/course.js");

// Function for adding a course
// Activity s39
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course.
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin

/* Original addCourse code
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error) => {

		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}
*/

module.exports.addCourse = (newData2) => {

	if(newData2.isAdmin == true){ 
		let newCourse = new Course({
			name: newData2.course.name,
			description: newData2.course.description,
			price: newData2.course.price,
			slots: newData2.course.slots
		})

		return newCourse.save().then((newCourse, error) => {

			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message2 = Promise.resolve('User must be ADMIN to add course');
		return message2.then((value) => {return value});
	}
}

// GET all courses
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

// GET specific course
module.exports.getCourse = (courseID) => {
	return Course.findById(courseID).then(result => {
		return result;
	})
}

// Updating a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

// Activity s40 - Archive a course
module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: newData.course.isActive 
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive course');
		return message.then((value) => {return value});
	}
}
