const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Creating a course
// Activity s39
// 1. Refractor the "course" route to implement user authentication for the admin when creating a course.

/* Original router.post code
router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
})
*/

router.post("/create", auth.verify, (request, response) => 
{
	const newData2 = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.addCourse(newData2)
	.then(resultFromController => response.send(resultFromController))
})

// Get all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})

// GET all Active Courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
});

// GET specific course
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
});

// Updating a course
router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

// Activity s40 - Archive a course
router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
}) 

module.exports = router;
